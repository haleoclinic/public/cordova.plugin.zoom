package cordova.plugin.zoom;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.IllformedLocaleException;
import java.util.Locale;
import java.util.Locale.Builder;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

import us.zoom.sdk.InstantMeetingOptions;
import us.zoom.sdk.JoinMeetingOptions;
import us.zoom.sdk.JoinMeetingParams;
import us.zoom.sdk.MeetingError;
import us.zoom.sdk.MeetingParameter;
import us.zoom.sdk.MeetingService;
import us.zoom.sdk.MeetingServiceListener;
import us.zoom.sdk.MeetingStatus;
import us.zoom.sdk.MeetingViewsOptions;
import us.zoom.sdk.SDKNotificationServiceError;
import us.zoom.sdk.StartMeetingOptions;
import us.zoom.sdk.StartMeetingParams4NormalUser;
import us.zoom.sdk.StartMeetingParamsWithoutLogin;
import us.zoom.sdk.ZoomApiError;
import us.zoom.sdk.ZoomAuthenticationError;
import us.zoom.sdk.ZoomSDK;
import us.zoom.sdk.ZoomSDKAuthenticationListener;

/**
 * Zoom
 * <p>
 * A Cordova Plugin to use Zoom Video Conferencing services on Cordova applications.
 *
 * @author Zoom Video Communications, Inc.
 * @version v5.7.6
 */
@SuppressLint("LongLogTag")
public class Zoom extends CordovaPlugin implements ZoomSDKAuthenticationListener, MeetingServiceListener {
    /* Debug variables */
    private static final String TAG = "<------- ZoomIonicAngularPlugin ---------->";
    private static final boolean DEBUG = false;
    public static final Object LOCK = new Object();

    private String WEB_DOMAIN = "https://zoom.us";

    private CallbackContext callbackContext;

    /**
     * execute
     * <p>
     * The bridging method to get parameters from JavaScript to execute the relevant Java methods.
     *
     * @param action          action name.
     * @param args            arguments.
     * @param callbackContext callback context.
     * @return true if everything runs smooth / false if something is wrong.
     * @throws JSONException might throw exceptions when parsing JSON arrays and objects.
     */

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext)
    throws JSONException {
    if (DEBUG) {
        Log.v(TAG, "----- [execute , action =" + action + "]");
        Log.v(TAG, "----- [execute , args =" + args + "]");
    }

    cordova.setActivityResultCallback(this);
    this.callbackContext = callbackContext;

    switch (action) {
    case "initialize":
        String jwtToken = args.getString(0);
        this.initialize(jwtToken, callbackContext);
        break;
    case "joinMeeting":
        String meetingNo = args.getString(0);
        String meetingPassword = args.getString(1);
        String displayNameJ = args.getString(2);
        JSONObject optionsJ = args.getJSONObject(3);
        this.joinMeeting(meetingNo, meetingPassword, displayNameJ, optionsJ, callbackContext);
        break;
    case "setLocale":
        String languageTag = args.getString(0);
        this.setLocale(languageTag, callbackContext);
        break;
    case "getMeetingStatus":
        this.getMeetingStatus(callbackContext);
        break;
    default:
        return false;
    }
    return true;
}

/**
 * initialize
 * <p>
 * Initialize Zoom SDK.
 *
 * @param jwtToken        Zoom JWT Token.
 * @param callbackContext Cordova callback context.
 */
private void initialize(String jwtToken, CallbackContext callbackContext) {
    if (DEBUG) {
        Log.v(TAG, "********** Zoom's initialize called **********");
    }

    FutureTask<Boolean> isInitializedTask = new FutureTask<>(new Callable<Boolean>() {
    @Override
    public Boolean call() {
            return ZoomSDK.getInstance().isInitialized();
        }
    });
    cordova.getActivity().runOnUiThread(isInitializedTask);

    // If the SDK has been successfully initialized, simply return.
    try {
        if (isInitializedTask.get()) {
            return;
        }
    } catch (ExecutionException | InterruptedException e) {
        Log.e(TAG, "error", e);
        callbackContext.error("Error checking if SDK is initialized: " + e.getMessage());
        return;
    }

    // Note: When "null" is pass from JS to Android, it is transferred as a word "null".
    if (jwtToken == null || jwtToken.trim().isEmpty() || jwtToken.equals("null")) {
        callbackContext.error("JWT Token cannot be empty");
        return;
    }


    try {
        AuthThread at = new AuthThread();                           // Prepare Auth Thread
        at.setCordova(cordova);                                     // Set cordova
        at.setCallbackContext(callbackContext);                     // Set callback context
        at.setAction("initialize");                                 // Set action
        at.setLock(LOCK);
        at.setInitParameters(jwtToken, this.WEB_DOMAIN);            // Set init parameters
        FutureTask<Boolean> fr = new FutureTask<Boolean>(at);

        cordova.getActivity().runOnUiThread(fr);                    // Run init method on main thread

        boolean threadSuccess = fr.get();                           // False if has error.
        if (DEBUG) {
            Log.v(TAG, "******************Return from Future is: " + threadSuccess);
        }

        if (threadSuccess) {
            // Wait until the initialize result is back.
            synchronized (LOCK) {
                try {
                    if (DEBUG) {
                        Log.v(TAG, "Wait................................");
                    }
                    LOCK.wait();
                } catch (InterruptedException e) {
                    if (DEBUG) {
                        Log.v(TAG, e.getMessage());
                    }
                }
            }
        }

        callbackContext.success("Initialize successfully!");
    } catch (Exception e) {
        callbackContext.error(e.getMessage());
    }
}

/**
 * joinMeeting
 * <p>
 * Join a meeting
 *
 * @param meetingNo       meeting number.
 * @param meetingPassword meeting password
 * @param displayName     display name shown in meeting.
 * @param option          meeting options.
 * @param callbackContext cordova callback context.
 */
private void joinMeeting(String meetingNo, String meetingPassword, String displayName, JSONObject option, CallbackContext callbackContext) {

    if (DEBUG) {
        Log.v(TAG, "********** Zoom's join meeting called ,meetingNo=" + meetingNo + " **********");
    }

    if (meetingNo == null || meetingNo.trim().isEmpty() || meetingNo.equals("null")) {
        callbackContext.error("Meeting number cannot be empty");
        return;
    }

    String meetingNumber = meetingNo.trim().replaceAll("[^0-9]", "");

    if (meetingNumber.length() < 9 || meetingNumber.length() > 11 || !meetingNumber.matches("\\d{8,11}")) {
        callbackContext.error("Please enter a valid meeting number.");
        return;
    }

    if (DEBUG) {
        Log.v(TAG, "[Going to Join Meeting]");
        Log.v(TAG, "[meetingNo=]" + meetingNumber);
    }
    // If the meeting number is invalid, throw error.
    if (meetingNumber.length() == 0) {
        PluginResult pluginResult = new PluginResult(PluginResult.Status.ERROR, "You need to enter a meeting number which you want to join.");
        pluginResult.setKeepCallback(true);
        callbackContext.sendPluginResult(pluginResult);
        return;
    }

    cordova.getActivity().runOnUiThread(() -> {
        // Get Zoom SDK instance.
        ZoomSDK zoomSDK = ZoomSDK.getInstance();
        PluginResult pluginResult;
        // If the Zoom SDK instance is not initialized, throw error.
        if (!zoomSDK.isInitialized()) {
            pluginResult = new PluginResult(PluginResult.Status.ERROR, "ZoomSDK has not been initialized successfully");
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);
            return;
        }

        // Get meeting service instance.
        MeetingService meetingService = zoomSDK.getMeetingService();

        // Configure join meeting parameters.
        JoinMeetingParams params = new JoinMeetingParams();
        params.displayName = displayName;
        params.meetingNo = meetingNumber;

        // Set meeting password.
        if (meetingPassword.length() > 0) {
            params.password = meetingPassword;
        }

        // if (option != null) {
        // If meeting option is provided, setup meeting options and join meeting.
        JoinMeetingOptions opts = new JoinMeetingOptions();
        try {

            if(option.getString("locale").equals("fr-ca")){
                zoomSDK.setSdkLocale( cordova.getActivity().getApplicationContext(), Locale.FRENCH);
            }else{
                zoomSDK.setSdkLocale( cordova.getActivity().getApplicationContext(), Locale.ENGLISH);
            }

            opts.custom_meeting_id = option.isNull("custom_meeting_id") ? null : option.getString("custom_meeting_id");
            opts.customer_key = option.isNull("customer_key") ? null : option.getString("customer_key");
            opts.no_unmute_confirm_dialog = option.isNull("no_unmute_confirm_dialog") ? false : option.getBoolean("no_unmute_confirm_dialog");
            opts.no_webinar_register_dialog = option.isNull("no_webinar_register_dialog") ? false : option.getBoolean("no_webinar_register_dialog");
            opts.no_driving_mode = option.isNull("no_driving_mode") ? false : option.getBoolean("no_driving_mode");
            opts.no_invite = option.isNull("no_invite") ? false : option.getBoolean("no_invite");
            opts.no_meeting_end_message = option.isNull("no_meeting_end_message") ? false : option.getBoolean("no_meeting_end_message");
            opts.no_titlebar = option.isNull("no_titlebar") ? false : option.getBoolean("no_titlebar");
            opts.no_bottom_toolbar = option.isNull("no_bottom_toolbar") ? false : option.getBoolean("no_bottom_toolbar");
            opts.no_dial_in_via_phone = option.isNull("no_dial_in_via_phone") ? false : option.getBoolean("no_dial_in_via_phone");
            opts.no_dial_out_to_phone = option.isNull("no_dial_out_to_phone") ? false : option.getBoolean("no_dial_out_to_phone");
            opts.no_disconnect_audio = option.isNull("no_disconnect_audio") ? false : option.getBoolean("no_disconnect_audio");
            opts.no_share = option.isNull("no_share") ? false : option.getBoolean("no_share");
            opts.no_audio = option.isNull("no_audio") ? false : option.getBoolean("no_audio");
            opts.no_video = option.isNull("no_video") ? false : option.getBoolean("no_video");
            opts.no_meeting_error_message = option.isNull("no_meeting_error_message") ? false : option.getBoolean("no_meeting_error_message");
            opts.meeting_views_options = 0;

            if (!option.isNull("no_button_video") && option.getBoolean("no_button_video")) {
                opts.meeting_views_options += MeetingViewsOptions.NO_BUTTON_VIDEO;
            }

            if (!option.isNull("no_button_audio") && option.getBoolean("no_button_audio")) {
                opts.meeting_views_options += MeetingViewsOptions.NO_BUTTON_AUDIO;
            }

            if (!option.isNull("no_button_share") && option.getBoolean("no_button_share")) {
                opts.meeting_views_options += MeetingViewsOptions.NO_BUTTON_SHARE;
            }

            if (!option.isNull("no_button_participants") && option.getBoolean("no_button_participants")) {
                opts.meeting_views_options += MeetingViewsOptions.NO_BUTTON_PARTICIPANTS;
            }

            if (!option.isNull("no_button_more") && option.getBoolean("no_button_more")) {
                opts.meeting_views_options += MeetingViewsOptions.NO_BUTTON_MORE;
            }

            if (!option.isNull("no_text_meeting_id") && option.getBoolean("no_text_meeting_id")) {
                opts.meeting_views_options += MeetingViewsOptions.NO_TEXT_MEETING_ID;
            }

            if (!option.isNull("no_text_password") && option.getBoolean("no_text_password")) {
                opts.meeting_views_options += MeetingViewsOptions.NO_TEXT_PASSWORD;
            }

            if (!option.isNull("no_button_leave") && option.getBoolean("no_button_leave")) {
                opts.meeting_views_options += MeetingViewsOptions.NO_BUTTON_LEAVE;
            }

            if (!option.isNull("no_button_switch_camera") && option.getBoolean("no_button_switch_camera")) {
                opts.meeting_views_options += MeetingViewsOptions.NO_BUTTON_SWITCH_CAMERA;
            }

            if (!option.isNull("no_button_switch_audio_source") && option.getBoolean("no_button_switch_audio_source")) {
                opts.meeting_views_options += MeetingViewsOptions.NO_BUTTON_SWITCH_AUDIO_SOURCE;
            }

        } catch (JSONException ex) {
            if (DEBUG) {
                Log.i(TAG, ex.getMessage());
            }
        }


        int response = meetingService.joinMeetingWithParams(
            cordova.getActivity().getApplicationContext(), params, opts);
        if (DEBUG) {
            Log.i(TAG, "In JoinMeeting, response=" + getMeetingErrorMessage(response));
        }
        if (response != MeetingError.MEETING_ERROR_SUCCESS) {
            pluginResult = new PluginResult(PluginResult.Status.ERROR, getMeetingErrorMessage(response));
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);
        } else {
            pluginResult = new PluginResult(PluginResult.Status.OK, getMeetingErrorMessage(response));
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);
        }

    });
}

/**
 * setLocale
 * <p>
 * Change the in-meeting language.
 *
 * @param languageTag     IETF BCP 47 language tag string
 * @param callbackContext cordova callback context
 */
private void setLocale(String languageTag, CallbackContext callbackContext) {
    try {
        cordova.getActivity().runOnUiThread(new Runnable() {
        @Override
        public void run() {
                if (DEBUG) {
                    Log.v(TAG, "[#############setLocale Thread run()##############]");
                }
                ZoomSDK zoomSDK = ZoomSDK.getInstance();
                try {
                    Locale language = new Builder().setLanguageTag(languageTag.replaceAll("_", "-")).build();
                    zoomSDK.setSdkLocale(cordova.getActivity().getApplicationContext(), language);
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, "Successfully set language to " + languageTag));
                } catch (IllformedLocaleException ie) {
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "Please pass valid language and country codes. [ERROR:" + ie.getMessage() + "]"));
                }
            }
        });
    } catch (Exception e) {
        callbackContext.error(e.getMessage());
    }
}


/**
 * getApiErrorMessage
 * <p>
 * Get the error message of api process.
 *
 * @param errorCode error code.
 * @return A string message.
 */
private String getApiErrorMessage(int errorCode) {

    StringBuilder message = new StringBuilder();

    switch (errorCode) {
        case ZoomApiError.ZOOM_API_ERROR_FAILED_CLIENT_INCOMPATIBLE:
            message.append("Your Zoom SDK client is not compatible. " +
                "Please download the latest version and try again.");
            break;
        case ZoomApiError.ZOOM_API_ERROR_FAILED_NULLPOINTER:
            message.append("Zoom SDK was not initialized successfully. ");
            break;
        case ZoomApiError.ZOOM_API_ERROR_FAILED_WRONGPARAMETERS:
            message.append("The combination of your username and password does not match our record.");
            break;
        case ZoomApiError.ZOOM_API_INVALID_STATUS:
            message.append("Our API is taking a break. Please try again later.");
            break;
        default:
            message.append("You are already logged in.");
            break;
    }

    if (DEBUG) {
        Log.v(TAG, "******getAuthErrorMessage*********" + message.toString());
    }

    return message.toString();
}

/**
 * onZoomAuthIdentityExpired
 * <p>
 * A listener to get notified when the authentication identity has expired.
 */
@Override
public void onZoomAuthIdentityExpired() {
    Log.v(TAG, "onZoomAuthIdentityExpired is triggered");
}

@Override
public void onZoomSDKLoginResult(long l) {

}

@Override
public void onZoomSDKLogoutResult(long l) {

}

/**
 * onZoomIdentityExpired
 * <p>
 * A listener to log user out once identity is expired.
 */
@Override
public void onZoomIdentityExpired() {
    cordova.getActivity().runOnUiThread(() -> {
        if (ZoomSDK.getInstance().isLoggedIn()) {
            ZoomSDK.getInstance().logoutZoom();
        }
    });
}

@Override
public void onNotificationServiceStatus(SDKNotificationServiceStatus var1, SDKNotificationServiceError var2) {

}

/**
 * onMeetingStatusChanged
 * <p>
 * A listener to retrieve info when meeting status changed.
 *
 * @param meetingStatus     meeting status code.
 * @param errorCode         error code.
 * @param internalErrorCode internal error code.
 */
@Override
public void onMeetingStatusChanged(MeetingStatus meetingStatus, int errorCode,
    int internalErrorCode) {
    if (DEBUG) {
        Log.i(TAG, "onMeetingStatusChanged, meetingStatus=" + meetingStatus + ", errorCode=" + errorCode
            + ", internalErrorCode=" + internalErrorCode);
    }

    if (meetingStatus == MeetingStatus.MEETING_STATUS_FAILED && errorCode == MeetingError.MEETING_ERROR_CLIENT_INCOMPATIBLE) {
        final android.widget.Toast toast = android.widget.Toast.makeText(
            cordova.getActivity().getApplicationContext(),
            "Version of ZoomSDK is too low!",
            android.widget.Toast.LENGTH_LONG
        );
        toast.show();
    }
}

@Override
public void onMeetingParameterNotification(MeetingParameter meetingParameter) {

}

/**
 * getMeetingErrorMessage
 * <p>
 * Get meeting error message.
 *
 * @param errorCode error code.
 * @return A string message.
 */
private String getMeetingErrorMessage(int errorCode) {

    StringBuilder message = new StringBuilder();

    switch (errorCode) {
        case MeetingError.MEETING_ERROR_CLIENT_INCOMPATIBLE:
            message.append("Zoom SDK version is too low to connect to the meeting");
            break;
        case MeetingError.MEETING_ERROR_DISALLOW_HOST_REGISTER_WEBINAR:
            message.append("Cannot register a webinar using the host email");
            break;
        case MeetingError.MEETING_ERROR_DISALLOW_PANELIST_REGISTER_WEBINAR:
            message.append("Cannot register a webinar using a panelist's email");
            break;
        case MeetingError.MEETING_ERROR_EXIT_WHEN_WAITING_HOST_START:
            message.append("User leave meeting when waiting host to start");
            break;
        case MeetingError.MEETING_ERROR_HOST_DENY_EMAIL_REGISTER_WEBINAR:
            message.append("The register to this webinar is denied by the host");
            break;
        case MeetingError.MEETING_ERROR_INCORRECT_MEETING_NUMBER:
            message.append("Incorrect meeting number");
            break;
        case MeetingError.MEETING_ERROR_INVALID_ARGUMENTS:
            message.append("Failed due to one or more invalid arguments.");
            break;
        case MeetingError.MEETING_ERROR_INVALID_STATUS:
            message.append("Meeting api can not be called now.");
            break;
        case MeetingError.MEETING_ERROR_LOCKED:
            message.append("Meeting is locked");
            break;
        case MeetingError.MEETING_ERROR_MEETING_NOT_EXIST:
            message.append("Meeting dose not exist");
            break;
        case MeetingError.MEETING_ERROR_MEETING_OVER:
            message.append("Meeting ended");
            break;
        case MeetingError.MEETING_ERROR_MMR_ERROR:
            message.append("Server error");
            break;
        case MeetingError.MEETING_ERROR_NETWORK_ERROR:
            message.append("Network error");
            break;
        case MeetingError.MEETING_ERROR_NETWORK_UNAVAILABLE:
            message.append("Network unavailable");
            break;
        case MeetingError.MEETING_ERROR_NO_MMR:
            message.append("No server is available for this meeting");
            break;
        case MeetingError.MEETING_ERROR_REGISTER_WEBINAR_FULL:
            message.append("Arrive maximum registers to this webinar");
            break;
        case MeetingError.MEETING_ERROR_RESTRICTED:
            message.append("Meeting is restricted");
            break;
        case MeetingError.MEETING_ERROR_RESTRICTED_JBH:
            message.append("Join this meeting before host is restricted");
            break;
        case MeetingError.MEETING_ERROR_SESSION_ERROR:
            message.append("Session error");
            break;
        case MeetingError.MEETING_ERROR_SUCCESS:
            message.append("Success");
            break;
        case MeetingError.MEETING_ERROR_TIMEOUT:
            message.append("Timeout");
            break;
        case MeetingError.MEETING_ERROR_UNKNOWN:
            message.append("Unknown error");
            break;
        case MeetingError.MEETING_ERROR_USER_FULL:
            message.append("Number of participants is full.");
            break;
        case MeetingError.MEETING_ERROR_WEB_SERVICE_FAILED:
            message.append("Request to web service failed.");
            break;
        case MeetingError.MEETING_ERROR_WEBINAR_ENFORCE_LOGIN:
            message.append("This webinar requires participants to login.");
            break;
        default:
            break;
    }

    if (DEBUG) {
        Log.v(TAG, "******getMeetingErrorMessage*********" + message.toString());
    }
    return message.toString();
}

/**
 * getMeetingStatus
 * <p>
 * Get meeting status
 *
 * @param callbackContext cordova callback context.
 */
private void getMeetingStatus(CallbackContext callbackContext) {

    cordova.getActivity().runOnUiThread(() -> {

        // Get Zoom SDK instance.
        ZoomSDK
        zoomSDK = ZoomSDK.getInstance();

        // Get meeting service instance.
        MeetingService
        meetingService = zoomSDK.getMeetingService();

        callbackContext.success(meetingService.getMeetingStatus().toString());
    });
}

}
