/**
 *  Zoom.m
 *
 *  @author Zoom Video Communications, Inc.
 *  @version v5.7.1
 */

#import "Zoom.h"

#define kSDKDomain  @"https://zoom.us"
#define DEBUG   NO

@implementation Zoom

- (void)initialize:(CDVInvokedUrlCommand*)command
{
    pluginResult = nil;
    callbackId = command.callbackId;
    // Get variables.
    NSString* jwtToken = [command.arguments objectAtIndex:0];

    // Run authentication and initialize SDK on main thread.
    dispatch_async(dispatch_get_main_queue(), ^(void){
        // if input parameters are not valid.
        if (jwtToken == nil || ![jwtToken isKindOfClass:[NSString class]] || [jwtToken length] == 0) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Please pass valid JWT Token"];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
            return;
        }

        // Initialize SDK.
        MobileRTCSDKInitContext *context = [[MobileRTCSDKInitContext alloc] init];
        context.domain = kSDKDomain;
        context.enableLog = YES;
        context.locale = MobileRTC_ZoomLocale_Default;

        BOOL initRet = [[MobileRTC sharedRTC] initialize:context];

        // If the SDK has successfully authorized, avoid re-authorization.
        if ([[MobileRTC sharedRTC] isRTCAuthorized])
        {
            return;
        }

        // Get auth service.
        MobileRTCAuthService *authService = [[MobileRTC sharedRTC] getAuthService];
        if (authService)
        {
            // Assign delegate.
            authService.delegate = self;
            // Assign jwtToken.
            authService.jwtToken = jwtToken;
            // Perform SDK auth.
            [authService sdkAuth];
        }

        [[[MobileRTC sharedRTC] getMeetingSettings] disableClearWebKitCache:YES];
    });
}

- (void)joinMeeting:(CDVInvokedUrlCommand*)command
{
    pluginResult = nil;
    callbackId = command.callbackId;
    // Get variables.
    NSString* meetingNo = [command.arguments objectAtIndex:0];
    NSString* meetingPassword = [command.arguments objectAtIndex:1];
    NSString* displayName = [command.arguments objectAtIndex:2];
    NSDictionary* options = [command.arguments objectAtIndex:3];

    if (DEBUG) {
        NSLog(@"========meeting number======= %@", meetingNo);
        NSLog(@"========display name======= %@", displayName);
        NSLog(@"========meeting options===== %@", options);
    }
    // Meeting number regular expression.
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\d{8,11}" options:0 error:nil];

    dispatch_async(dispatch_get_main_queue(), ^(void) {
        if (meetingNo == nil || ![meetingNo isKindOfClass:[NSString class]] || [meetingNo length] == 0 || [regex numberOfMatchesInString:meetingNo options:0 range:NSMakeRange(0, [meetingNo length])] == 0|| displayName == nil || ![displayName isKindOfClass:[NSString class]] || [displayName length] == 0) {
            NSLog(@"Please enter valid meeting number and display name");
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Please enter valid meeting number and display name"];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
            return;
        }
        // Get meeting service
        MobileRTCMeetingService *ms = [[MobileRTC sharedRTC] getMeetingService];
        if (ms != nil)
        {
            // Assign delegate.
            ms.delegate = self;
            NSString* customerKey = @"";
            // Meeting options
            if (options != nil) {
                // custom_meeting_id
                if ([options objectForKey:@"custom_meeting_id"] != nil) {
                    NSString* customMeetingId = options[@"custom_meeting_id"];
                    [ms customizeMeetingTitle:customMeetingId];
                }
                // no_audio
                if ([options objectForKey:@"no_audio"] != nil) {
                    [[[MobileRTC sharedRTC] getMeetingSettings] setMuteAudioWhenJoinMeeting:[options[@"no_audio"] boolValue]];
                } else {
                    [[[MobileRTC sharedRTC] getMeetingSettings] setMuteAudioWhenJoinMeeting:NO];
                }
                // no_bottom_toolbar
                if ([options objectForKey:@"no_bottom_toolbar"] != nil) {
                    [[MobileRTC sharedRTC] getMeetingSettings].bottomBarHidden = [options[@"no_bottom_toolbar"] boolValue];
                } else {
                    [[MobileRTC sharedRTC] getMeetingSettings].bottomBarHidden = NO;
                }
                // no_dial_in_via_phone
                if ([options objectForKey:@"no_dial_in_via_phone"] != nil) {
                    [[[MobileRTC sharedRTC] getMeetingSettings] disableCallIn: [options[@"no_dial_in_via_phone"] boolValue]];
                } else {
                    [[[MobileRTC sharedRTC] getMeetingSettings] disableCallIn: NO];
                }
                // no_dial_out_to_phone
                if ([options objectForKey:@"no_dial_out_to_phone"] != nil) {
                    [[[MobileRTC sharedRTC] getMeetingSettings] disableCallOut: [options[@"no_dial_out_to_phone"] boolValue]];
                } else {
                    [[[MobileRTC sharedRTC] getMeetingSettings] disableCallOut:NO];
                }
                // no_disconnect_audio
                if ([options objectForKey:@"no_disconnect_audio"] != nil) {
                    [[MobileRTC sharedRTC] getMeetingSettings ].disconnectAudioHidden = [options[@"no_disconnect_audio"] boolValue];
                } else {
                    [[MobileRTC sharedRTC] getMeetingSettings ].disconnectAudioHidden = NO;
                }

                // no_driving_mode
                if ([options objectForKey:@"no_driving_mode"] != nil) {
                    [[[MobileRTC sharedRTC] getMeetingSettings] disableDriveMode: [options[@"no_driving_mode"] boolValue]];
                } else {
                    [[[MobileRTC sharedRTC] getMeetingSettings] disableDriveMode:NO];
                }
                // no_invite
                if ([options objectForKey:@"no_invite"] != nil) {
                    [[MobileRTC sharedRTC] getMeetingSettings].meetingInviteHidden = [options[@"no_invite"] boolValue];
                } else {
                    [[MobileRTC sharedRTC] getMeetingSettings].meetingInviteHidden = NO;
                }

                // no_titlebar
                if ([options objectForKey:@"no_titlebar"] != nil) {
                    [[MobileRTC sharedRTC] getMeetingSettings].topBarHidden = [options[@"no_titlebar"] boolValue];
                } else {
                    [[MobileRTC sharedRTC] getMeetingSettings].topBarHidden = NO;
                }

                // no_video
                if ([options objectForKey:@"no_video"] != nil) {
                    [[[MobileRTC sharedRTC] getMeetingSettings] setMuteVideoWhenJoinMeeting:[options[@"no_video"] boolValue]];
                } else {
                    [[[MobileRTC sharedRTC] getMeetingSettings] setMuteVideoWhenJoinMeeting:NO];
                }

                // no_button_video
                if ([options objectForKey:@"no_button_video"] != nil) {
                    [[MobileRTC sharedRTC] getMeetingSettings].meetingVideoHidden = [options[@"no_button_video"] boolValue];
                } else {
                    [[MobileRTC sharedRTC] getMeetingSettings].meetingVideoHidden = NO;
                }
                // no_button_audio
                if ([options objectForKey:@"no_button_audio"] != nil) {
                    [[MobileRTC sharedRTC] getMeetingSettings].meetingAudioHidden = [options[@"no_button_audio"] boolValue];
                } else {
                    [[MobileRTC sharedRTC] getMeetingSettings].meetingAudioHidden = NO;
                }
                // no_button_share
                if ([options objectForKey:@"no_button_share"] != nil) {
                    [[MobileRTC sharedRTC] getMeetingSettings].meetingShareHidden = [options[@"no_button_share"] boolValue];
                } else {
                    [[MobileRTC sharedRTC] getMeetingSettings].meetingShareHidden = NO;
                }
                // no_button_participants
                if ([options objectForKey:@"no_button_participants"] != nil) {
                    [[MobileRTC sharedRTC] getMeetingSettings].meetingParticipantHidden = [options[@"no_button_participants"] boolValue];
                } else {
                    [[MobileRTC sharedRTC] getMeetingSettings].meetingParticipantHidden = NO;
                }
                // no_button_more
                if ([options objectForKey:@"no_button_more"] != nil) {
                    [[MobileRTC sharedRTC] getMeetingSettings].meetingMoreHidden = [options[@"no_button_more"] boolValue];
                } else {
                    [[MobileRTC sharedRTC] getMeetingSettings].meetingMoreHidden = NO;
                }
                // no_text_meeting_id
                if ([options objectForKey:@"no_text_meeting_id"] != nil) {
                    [[MobileRTC sharedRTC] getMeetingSettings].meetingTitleHidden = [options[@"no_text_meeting_id"] boolValue];
                } else {
                    [[MobileRTC sharedRTC] getMeetingSettings].meetingTitleHidden = NO;
                }
                // no_text_password
                if ([options objectForKey:@"no_text_password"] != nil) {
                    [[MobileRTC sharedRTC] getMeetingSettings].meetingPasswordHidden = [options[@"no_text_password"] boolValue];
                } else {
                    [[MobileRTC sharedRTC] getMeetingSettings].meetingPasswordHidden = NO;
                }
                // no_button_leave
                if ([options objectForKey:@"no_button_leave"] != nil) {
                    [[MobileRTC sharedRTC] getMeetingSettings].meetingLeaveHidden = [options[@"no_button_leave"] boolValue];
                } else {
                    [[MobileRTC sharedRTC] getMeetingSettings].meetingLeaveHidden = NO;
                }
                // customer_key
                if ([options objectForKey:@"customer_key"] != nil) {
                    customerKey = options[@"customer_key"];
                }

                // locale
                if ([[options objectForKey:@"locale"] isEqualToString:@"fr-ca"]) {
                    [[MobileRTC sharedRTC] setLanguage:@"fr"];
                }else{
                    [[MobileRTC sharedRTC] setLanguage:@"en"];
                }
            }

            [[[MobileRTC sharedRTC] getMeetingSettings] disableClearWebKitCache:YES];
            [[[MobileRTC sharedRTC] getMeetingSettings] disableShowVideoPreviewWhenJoinMeeting:YES];
            [[MobileRTC sharedRTC] getMeetingSettings].autoConnectInternetAudio = YES;

            MobileRTCMeetingJoinParam * joinParam = [[MobileRTCMeetingJoinParam alloc] init];
            joinParam.userName = displayName;
            joinParam.meetingNumber = meetingNo;
            joinParam.password = meetingPassword;
            joinParam.customerKey = customerKey;
            // Join meeting.
            MobileRTCMeetError response = [ms joinMeetingWithJoinParam:joinParam];
            if (DEBUG) {
                NSLog(@"Join a Meeting res:%lu", response);
            }
        }
    });
}

- (void)setLocale:(CDVInvokedUrlCommand *)command
{
    pluginResult = nil;
    callbackId = command.callbackId;
    // Get variable
    NSString* languageTag = [command.arguments objectAtIndex:0];

    NSString* language = @"en";

    // Ugly way to unify language codes.
    if ([languageTag isEqualToString:@"en-US"]) {
        language = @"en";
    } else if ([languageTag isEqualToString:@"zh-CN"]) {
        language = @"zh-Hans";
    } else if ([languageTag isEqualToString:@"ja-JP"]) {
        language = @"ja";
    } else if ([languageTag isEqualToString:@"de-DE"]) {
        language = @"de";
    } else if ([languageTag isEqualToString:@"fr-FR"]) {
        language = @"fr";
    } else if ([languageTag isEqualToString:@"zh-TW"]) {
        language = @"zh-Hant";
    } else if ([languageTag isEqualToString:@"es-419"]) {
        language = @"es";
    } else if ([languageTag isEqualToString:@"ru-RU"]) {
        language = @"ru";
    } else if ([languageTag isEqualToString:@"pt-PT"]) {
        language = @"pt-PT";
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Language not supported"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
        return;
    }

    dispatch_async(dispatch_get_main_queue(), ^(void) {
        if (DEBUG) {
            NSLog(@"set language ===== %@", language);
            NSLog(@"Supported languages: %@", [[MobileRTC sharedRTC] supportedLanguages]);
        }
        // Set language
        [[MobileRTC sharedRTC] setLanguage:language];

        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Set language Successfully"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    });
}

- (void)getMeetingStatus:(CDVInvokedUrlCommand*)command
{
    pluginResult = nil;
    callbackId = command.callbackId;
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        MobileRTCMeetingState state = [[[MobileRTC sharedRTC] getMeetingService] getMeetingState];

        if (DEBUG) {
            NSLog(@"status: %@", [self getStatusMessage:state]);
        }

        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[self getStatusMessage:state]];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    });
}

- (void)onMobileRTCAuthReturn:(MobileRTCAuthError)returnValue
{
    if (DEBUG) {
        NSLog(@"onMobileRTCAuthReturn: %@", [self getAuthErrorMessage:returnValue]);
    }

    if (returnValue != MobileRTCAuthError_Success)
    {
        // Authentication error, please check error code.
        NSString *message = [NSString stringWithFormat:NSLocalizedString(@"SDK authentication failed, error: %@", @""), [self getAuthErrorMessage:returnValue]];
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:message];
    } else {
        NSString *message = [NSString stringWithFormat:NSLocalizedString(@"Initialize successfully, return value: %@", @""), [self getAuthErrorMessage:returnValue]];
        NSLog(@"%@", message);
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:message];
    }
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
}

- (void)onMeetingError:(MobileRTCMeetError)error message:(NSString*)message
{
    if (DEBUG) {
     NSLog(@"onMeetingError:%zd, message:%@", error, message);
    }
    if (error != MobileRTCMeetError_Success) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:[self getMeetErrorMessage:error]];
    } else {
     pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[self getMeetErrorMessage:error]];
    }
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
}

- (NSString *)getAuthErrorMessage:(MobileRTCAuthError)errorCode
{
    NSString* message = @"";
    switch (errorCode) {
        case MobileRTCAuthError_Success:
            message = @"Authentication success.";
            break;
        case MobileRTCAuthError_KeyOrSecretEmpty:
            message = @"SDK key or secret is empty.";
            break;
        case MobileRTCAuthError_KeyOrSecretWrong:
            message = @"SDK key or secret is wrong.";
            break;
        case MobileRTCAuthError_AccountNotSupport:
            message = @"Your account does not support SDK.";
            break;
        case MobileRTCAuthError_AccountNotEnableSDK:
            message = @"Your account does not support SDK.";
            break;
        case MobileRTCAuthError_Unknown:
            message = @"Unknown error.Please try again.";
            break;
        default:
            message = @"Unknown error.Please try again.";
            break;
    }
    return message;
}


- (NSString *)getStatusMessage:(MobileRTCMeetingState)state
{
    NSString* message = @"";
    switch (state) {
        case MobileRTCMeetingState_Idle:
            message = @"MEETING_STATUS_IDLE";
            break;
        case MobileRTCMeetingState_Connecting:
            message = @"MEETING_STATUS_CONNECTING";
            break;
        case MobileRTCMeetingState_WaitingForHost:
            message = @"MEETING_STATUS_WAITINGFORHOST";
            break;
        case MobileRTCMeetingState_InMeeting:
            message = @"MEETING_STATUS_INMEETING";
            break;
        case MobileRTCMeetingState_Disconnecting :
            message = @"MEETING_STATUS_DISCONNECTING";
            break;
        case MobileRTCMeetingState_Reconnecting:
            message = @"MEETING_STATUS_RECONNECTING";
            break;
        case MobileRTCMeetingState_Failed:
            message = @"MEETING_STATUS_FAILED";
            break;
        case MobileRTCMeetingState_Ended:
            message = @"MEETING_STATUS_ENDED";
            break;
        case MobileRTCMeetingState_Locked:
            message = @"MEETING_STATUS_LOCKED";
            break;
        case MobileRTCMeetingState_Unlocked:
            message = @"MEETING_STATUS_UNLOCKED";
            break;
        case MobileRTCMeetingState_InWaitingRoom:
            message = @"MEETING_STATUS_IN_WAITING_ROOM";
            break;
        case MobileRTCMeetingState_WebinarPromote:
            message = @"MEETING_STATUS_WEBINAR_PROMOTE";
            break;
        case MobileRTCMeetingState_WebinarDePromote:
            message = @"MEETING_STATUS_WEBINAR_DEPROMOTE";
            break;
        case MobileRTCMeetingState_JoinBO:
            message = @"MEETING_STATUS_JOIN_BREAKOUT_ROOM";
            break;
        case MobileRTCMeetingState_LeaveBO:
            message = @"MEETING_STATUS_LEAVE_BREAKOUT_ROOM";
            break;
        default:
            message = @"MEETING_STATUS_UNKNOWN";
            break;
    }
    return message;
}

- (NSString *)getMeetErrorMessage:(MobileRTCMeetError)errorCode
{
    NSString * message = @"";
    switch (errorCode) {
        case MobileRTCMeetError_Success:
            message = @"Successfully start/join meeting.";
            break;
        case MobileRTCMeetError_NetworkError:
            message = @"Network issue, please check your network connection.";
            break;
        case MobileRTCMeetError_ReconnectError:
            message = @"Failed to reconnect to meeting.";
            break;
        case MobileRTCMeetError_MMRError:
            message = @"MMR issue, please check mmr configruation.";
            break;
        case MobileRTCMeetError_PasswordError:
            message = @"Meeting password incorrect.";
            break;
        case MobileRTCMeetError_SessionError:
            message = @"Failed to create a session with our sever.";
            break;
        case MobileRTCMeetError_MeetingOver:
            message = @"The meeting is over.";
            break;
        case MobileRTCMeetError_MeetingNotStart:
            message = @"The meeting does not start.";
            break;
        case MobileRTCMeetError_MeetingNotExist:
            message = @"The meeting does not exist.";
            break;
        case MobileRTCMeetError_MeetingUserFull:
            message = @"The meeting has reached a maximum of participants.";
            break;
        case MobileRTCMeetError_MeetingClientIncompatible:
            message = @"The Zoom SDK version is incompatible.";
            break;
        case MobileRTCMeetError_NoMMR:
            message = @"No mmr is available at this point.";
            break;
        case MobileRTCMeetError_MeetingLocked:
            message = @"The meeting is locked by the host.";
            break;
        case MobileRTCMeetError_MeetingRestricted:
            message = @"The meeting is restricted.";
            break;
        case MobileRTCMeetError_MeetingRestrictedJBH:
            message = @"The meeting does not allow join before host. Please try again later.";
            break;
        case MobileRTCMeetError_CannotEmitWebRequest:
            message = @"Failed to send create meeting request to server.";
            break;
        case MobileRTCMeetError_CannotStartTokenExpire:
            message = @"Failed to start meeting due to token exipred.";
            break;
        case MobileRTCMeetError_VideoError:
            message = @"The user's video cannot work.";
            break;
        case MobileRTCMeetError_AudioAutoStartError:
            message = @"The user's audio cannot auto start.";
            break;
        case MobileRTCMeetError_RegisterWebinarFull:
            message = @"The webinar has reached its maximum allowed participants.";
            break;
        case MobileRTCMeetError_RegisterWebinarHostRegister:
            message = @"Sign in to start the webinar.";
            break;
        case MobileRTCMeetError_RegisterWebinarPanelistRegister:
            message = @"Join the webinar from the link";
            break;
        case MobileRTCMeetError_RegisterWebinarDeniedEmail:
            message = @"The host has denied your webinar registration.";
            break;
        case MobileRTCMeetError_RegisterWebinarEnforceLogin:
            message = @"The webinar requires sign-in with specific account to join.";
            break;
        case MobileRTCMeetError_ZCCertificateChanged:
            message = @"The certificate of ZC has been changed. Please contact Zoom for further support.";
            break;
        case MobileRTCMeetError_VanityNotExist:
            message = @"The vanity does not exist";
            break;
        case MobileRTCMeetError_JoinWebinarWithSameEmail:
            message = @"The email address has already been register in this webinar.";
            break;
        case MobileRTCMeetError_WriteConfigFile:
            message = @"Failed to write config file.";
            break;
        case MobileRTCMeetError_RemovedByHost:
            message = @"You have been removed by the host.";
            break;
        case MobileRTCMeetError_InvalidArguments:
            message = @"Invalid arguments.";
            break;
        case MobileRTCMeetError_InvalidUserType:
            message = @"Invalid user type.";
            break;
        case MobileRTCMeetError_InAnotherMeeting:
            message = @"Already in another ongoing meeting.";
            break;
        case MobileRTCMeetError_Unknown:
            message = @"Unknown error.";
            break;
        default:
            message = @"Unknown error.";
            break;
    }
    return message;
}
@end
