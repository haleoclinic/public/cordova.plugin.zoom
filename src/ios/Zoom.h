/**
 *  Zoom.h
 *
 *  @author Carson Chen (carson.chen@zoom.us)
 *  @version v5.7.1
 */

#import <Cordova/CDV.h>
#import <MobileRTC/MobileRTC.h>
#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonHMAC.h>
/**
 *  Zoom
 *
 *  A Cordova Plugin to use Zoom Video Conferencing services on Cordova applications.
 */
@interface Zoom :  CDVPlugin<MobileRTCAuthDelegate, MobileRTCMeetingServiceDelegate> {
    NSString *callbackId;
    CDVPluginResult* pluginResult;
}
/**
 * initialize
 *
 * Initialize Zoom SDK.
 * @param CDVInvokedUrlCommand bridging method to get arguments, callbackId, className and methodName.
 */
- (void)initialize:(CDVInvokedUrlCommand*)command;
/**
 * joinMeeting
 *
 * Join a meeting.
 * @param CDVInvokedUrlCommand bridging method to get arguments, callbackId, className and methodName.
 */
- (void)joinMeeting:(CDVInvokedUrlCommand*)command;
/**
 * setLocale
 *
 * Set in-meeting language.
 * @param CDVInvokedUrlCommand bridging method to get arguments, callbackId, className and methodName.
 */
- (void)setLocale:(CDVInvokedUrlCommand*)command;
/**
 * getMeetingStatus
 *
 * Get meeting status.
  * @param CDVInvokedUrlCommand bridging method to get arguments, callbackId, className and methodName.
 */
- (void)getMeetingStatus:(CDVInvokedUrlCommand*)command;

@end
